const XP = 2500000; // 2.5 Mi
const KAMAS = 43980;

module.exports = {
  XP,
  KAMAS,
};
