const api = async (url) => {
  const baseUrl = "https://api.dofusdb.fr/";
  return fetch(url, {
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
    },
    redirect: "follow",
  }).then((response) => response.json());
};

module.exports = api;
