// Vendors
const { Client, GatewayIntentBits } = require("discord.js");
// Local data
const config = require("../config.json");
const data = require("../data.json");
// Imports
const constants = require("./constants.js");
const api = require("./api");
const { normalize } = require("./utils");
const { findQuest } = require("./almanax");

const discordClient = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.GuildMembers,
  ],
});

const getLastMessage = (channel) => {
  return channel.messages
    .fetch({ limit: 1 })
    .then((messages) => messages.first());
};

discordClient.on("ready", async (client) => {
  const channel = client.channels.cache.get(config.CHANNEL_ID);
  const lastMessage = await getLastMessage(channel);
  const meridia = "Hello";
  if (lastMessage.content.includes(meridia)) return;

  channel.send("Hello here!");
  console.log("Message sent!");
});

discordClient.login(config.TOKEN);
